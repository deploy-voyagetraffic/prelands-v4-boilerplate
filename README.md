## Prelands

<a target="_blank" href="https://opensource.org/licenses/MIT" title="License: MIT">
  <img src="https://img.shields.io/badge/License-MIT-blue.svg">
</a>
<a href="#badge">
  <img alt="code style: prettier" src="https://img.shields.io/badge/code_style-prettier-ff69b4.svg">
</a>
<a target="_blank" href="http://makeapullrequest.com" title="PRs Welcome"><img src="https://img.shields.io/badge/PRs-welcome-brightgreen.svg"></a>

> В качестве отправной точки необходимо взять текущий webpack+jquery boilerplate, в котором реализованы все требования.

```
node v14.15.4 
npm v6.14.10
nvm v0.35.3
```

Перед началом разработки одного преленда необходимо создать ветку с именем в формате 
`feature/<your_nickname>_<порядковый_номер_лендинга_из_Excel>`, например: `feature/webmaster_01`,
и результат (в том числе и zip-архив) пушить в неё (ssh ключи в конце данного README). 

Каждую новую ветку заводить от ветки `master`.

Таким образом на одну ветку будет один готовый преленд.

Команды `package.json`:
* `npm i` или `yarn install` - установка зависисмостей
* `npm run dev` - запуск webpack-dev-server
* `yarn build` - билд исходников в `dist/` директорию

Конечный результат должен быть упакован в zip-архив. В корне zip-архива должны находится все файлы из директории dist,
(т.е. вариант, когда в корне zip-архива находится одна директория, в которой находятся файлы - не подходит), например:
```
result.zip
         ├── images
         │   ├── bg-1-desktop.jpg
         │   ├── bg-1.jpg
         │   └── love.webp
         ├── icon.ico         
         ├── index.css
         ├── index.js
         ├── index_en.html
         └── index_ru.html
```
Пример архива: <https://gitlab.com/deploy-voyagetraffic/prelands-v4-boilerplate/-/blob/master/dist.zip>

Расширения файлов должны удовлетворять регулярному выражению: `/\.(gif|jpe?g|tiff?|png|webp|mov|avi|wmv|flv|3gp|mp4|mpg|css|js|ico|icon)/`.

##### Процесс сдачи результата

После того, как работа над одним прелендом завершена, результат (zip-архив) должен быть проверен Вами на странице <https://app.paysale.com/validate?it>.
При отсутствии ошибок валидации результат (zip-архив + все файлы) необходимо `git push` в ветку этого преленда и ссылку на zip-архив разместить в соответсвующей графе в Excel на GoogleDocs.
Каждый `git push` инициирует запуск процесса валидации в CI и преленд валиден, только если тест passed — <https://gitlab.com/deploy-voyagetraffic/prelands-v4-boilerplate/-/pipelines>.

##### Требования к исходникам

###### Images

1. Все используемые изображения хранятся в директории `images` (без поддиректорий)
2. Использование изображений, которые хранятся на неких внешних хостингах, недопустимо
3. Если JS\CSS вендор использует изображения с внешних хостингов - это допустимо
4. Можно хранить изображения в base64 внутри css.
5. Если используется видео - то файл в формате mp4,mpeg не превышает 4мб и хранится внутри директории `images`.
6. favicon `icon.ico` лежит в корне рядом с html файлами.
7. Предпочтительный формат для больших качественных изображений - `webp`. (online-converter: <https://image.online-convert.com/convert-to-webp>).
8. svg только в виде inline внутри css. Пример трансформации svg в поддерживаемый в css формат (javascript):
 ```javascript
 'data:image/svg+xml;charset=utf8,' + 
 '<svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg"><circle cx="50" cy="50" r="50"/></svg>'
   .split("<").join("%3C").split(">").join("%3E").split("#").join("%23")
 ```

###### HTML

1. Весь html 
    * содержится в единственном для выбранного языка `index_<locale>.html` файле
        * Например: `index_ru.html`, `index_en.html`, etc.
        * Список языков, для которых необходимо создать html, озвучивается при постановке задачи на разработку преленда.
2. Когда юзер кликает по ссылкам и кнопкам - контент меняется без перезагрузки страницы с помощью javascript (simple single page application без vue и react).
3. Должна быть кнопка `#continue`.
4. Не должен содержать inline javascript, css, images, только разметка и текст согласно локали.
5. должны отсутствовать элементы `<a>`, совершающие переход по ссылкам. Т.е. элементов `<a href="http://...">...</a>` быть не должно. 

###### JS

1. весь js приложения:
    1. должен быть внутри единственного js-файла `index.js`.
        * этот файл подключается перед закрывающим тэгом `</body>`.
    2. в архиве не должно быть никаких других `js` файлов.
2. js-вендоры:
    1. подключаются в `<head>` секции.
        * например: <br> `<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>`.
    2. либо содержатся внутри `index.js` в самом начале файла (т.е. вручную копируем и вставляем код в `index.js` из файлов вендоров).
3. JavaScript код не должен содержать манипуляций с урлом. Т.е. не должно быть: `location.href = ...`, `location.replace`, `location.assign`.

###### CSS

1. весь css приложения: 
    1. должен быть внутри файла `index.css`. 
        * этот файл подключается перед закрывающим тэгом `</head>`.
2. css-вендоры:
    1. подключаются в `<head>` секции.
    2. либо их код содержится внутри `index.css` в самом начале файла.
3. использование изображений в стилях:
    1. Относительный путь к директории `images`. Например: <br> `background-image: url(images/girl.jpg);`.
    2. можно использовать base64 или svg внутри css.
4. Вёрскта должна быть адаптивной вплоть до экрана айфона.
5. Не должно быть кастомных шрифтов вида `url("fonts/Gilroy-Bold.woff")`.
    
###### Fonts

Шрифты брать с googleapis. <br>
Шрифт должен поддерживать локаль преленда. <br>
Пример подключения в `<head>`: <br> `<link href="https://fonts.googleapis.com/css2?family=Roboto" rel="stylesheet">`

##### Функционал

###### Age block

Вопрос о возрасте может быть оформлен в виде (зависит от макета):

* вопрос "сколько вам лет" и поле ввода для указания возраста и кнопкой "submit"
* вопрос "укажите дату рождения" и три селекта с выбором дня, месяца, года и кнопкой "submit"
* вопрос "сколько вам лет" и радиокнопки с выбором диапазонов (диапазоны пронумерованы):
    * `0` n/a
    * `1` < 18
    * `2` 18 - 24
    * `3` 25 - 30
    * `4` 31 - 39
    * `5` 40 - 49
    * `6` 50+

Когда юзер выбирает пол, javascript должен вызвать функцию `post_age()` с одним параметром - идентификатором диапазона. 

Если юзеру меньше 18 лет - показываем сообщение о том, что регистрация возможна только для совершеннолетних.

Например, если было принято решение оформить блок с вопросом о возрасте в виде одого поля с указанием возраста, и юзер указал `25 лет`, при нажатии на кнопку submit должна быть вызвана функция `post_age(3)`.  

> Функцию `post_age` реализовывать не нужно, она подключится как часть фреймворка на этапе публикации преленда в интернете.

###### Gender block

Блок содержит вопрос о поле - мужчина (`1`) или женщина (`2`). Когда юзер выбирает пол, javascript должен вызвать функцию с одним параметром `post_gender(1)` или `post_gender(2)`.

> Функцию `post_gender` реализовывать не нужно, она подключится как часть фреймворка на этапе публикации преленда в интернете.

###### Email block

Поле для ввода email с кнопкой submit. Когда юзер вводит корректный email и нажимает submit - javascript должен вызвать функцию `post_email()` и передать в неё указанный email. 

> Функцию `post_email` реализовывать не нужно, она подключится как часть фреймворка на этапе публикации преленда в интернете.

###### Кнопка id="continue"

Её наличие обязательно, клик по ней будет обрабатываться фреймворком, после того, как преленд будет размещён в интернете.

#### ssh

Приватый ключ `cat ~/.ssh/id_rsa`:
```
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABlwAAAAdzc2gtcn
NhAAAAAwEAAQAAAYEAn0SAwc6oSiJIvHywBIXQnt/5t3i08rmKLucKx4IaJVmYmHoPlet6
rtqyx5QeiEFhBELF9WlFGCPNoOQ3QwuD6tvmWbxLw0NVzk6JpfeldheOwos91CX4z54kuf
DNnr8NEFFQS3npP2tVUFLfO3AEWvv0b30yA/WX+z+OZV/UEO4JZ8E1KHrkgbcmIlqcAhq/
/G0vpGItpotdDrtk9znS3V8lyOhNVj3uWObvdQn+q+mNL8OPYWcGoC2//dIVrOBBeKWDck
U3ZSlzlwLC3LYjSPglQFgXum/+jFhF63UyslGPaKrBfJPthetEd8h2EiH1dslguVkTiAzx
E61TUVkXcjItj1j5OjdswA/qLmQzKEJ1nMq/O3Pavg97p9MWJJUJ1RV/Nc5kMsSqTwjplj
u0tNGhoniy9cg3h59RlAzJLXkFqGyhSsfXB5N1i2KtI3mVqt3QTeD3/EeKuwq7cX0NVHWi
Q16QnnEbafT7k/hmEfcC8U/8GzfSUGCpl2s+10LNAAAFiGdBkSFnQZEhAAAAB3NzaC1yc2
EAAAGBAJ9EgMHOqEoiSLx8sASF0J7f+bd4tPK5ii7nCseCGiVZmJh6D5Xreq7asseUHohB
YQRCxfVpRRgjzaDkN0MLg+rb5lm8S8NDVc5OiaX3pXYXjsKLPdQl+M+eJLnwzZ6/DRBRUE
t56T9rVVBS3ztwBFr79G99MgP1l/s/jmVf1BDuCWfBNSh65IG3JiJanAIav/xtL6RiLaaL
XQ67ZPc50t1fJcjoTVY97ljm73UJ/qvpjS/Dj2FnBqAtv/3SFazgQXilg3JFN2Upc5cCwt
y2I0j4JUBYF7pv/oxYRet1MrJRj2iqwXyT7YXrRHfIdhIh9XbJYLlZE4gM8ROtU1FZF3Iy
LY9Y+To3bMAP6i5kMyhCdZzKvztz2r4Pe6fTFiSVCdUVfzXOZDLEqk8I6ZY7tLTRoaJ4sv
XIN4efUZQMyS15BahsoUrH1weTdYtirSN5lard0E3g9/xHirsKu3F9DVR1okNekJ5xG2n0
+5P4ZhH3AvFP/Bs30lBgqZdrPtdCzQAAAAMBAAEAAAGAAu0LQIQ/az0+ANh5k1ux/Xk36T
r6eAsYkswD0q59NHp05zg22RY+Zr/10NzTwsUQtzdS07NfZZYizPXkXgL0JXhsvvFKowe1
p9ihhr8J9yVvpftQ43jfHVFWc2QNIwF3h/k2dGyE8HlUU+lx2HGyYsmGMBYouRHl/OOqBg
rApp745UMTau73C7sFpFPt0cwUAtsuqOJ03D5rMz4WGMSjLxX0Z8B8yzzQCCoV9Bx+5UGh
KmN5Wty9rmuov00AwPBNvDHtV6gaFKxK+/Xtx3dDg/IMe8g9oXPDWQJc+w+6d6IEnsJ5dP
dfXYVoj1MCO4brkJKljFjnAhupsVX35P6ka23x8ofNuZ5z6DgDnSliWJuHRVVL26W8ZfV8
UrpslLIEuZt/vKdE9xpHsTxDbQky44eYWfty+5xztrF5IAu+8UiTUNvkaTNXCku/SIr5EK
i9o6RapmYlpJ6CPcOxLD8t3eHFuwu4TYwm8BrE3zxx0QPGHLtUIuuAJpydhwUJ1sCBAAAA
wQCOG1m0md9m5zCmE1bg9hApQ6005pg5qxhf4/Y7KXTGu/ct1fj8KqpAY541tJSAaxKbDD
sPZDVbMcV1+gpeH1/VgXt4PagSn/J7GQmqZdq8YntGTNlIBiqd593HMDbdwGAmBRStI6ef
IHkN+BOZjn3fMQkkVOboy1jemyU8ThIH70KLT0PZW7dH4nvRv35cd5g/5PLq1PoJaMQKlV
Tmn0zEwBqHEzSdL3eV9CJCp+6FlxiEDPfvbCXY0LmjiQTmqUUAAADBAM48gQMrcp0ALpId
ty08qP9YQts/b4g4UyWlI3pWr/wAkpZsQLp+hjxTC5uwHB/mNoL3w3Bvy2sPwSCBMFyd1n
fQlHMp6Q7nY5A8r4dNHLANPBGZzfnm4kaSr0nNbO/5bR9DGlGSR/NQ+HvTLbQmqgMxJIrz
dtE+20ZB2mrjeWt2SUBgQJKX9TxWn/K8kfxRAG0w+hind1POdyAdboTWlLDMhpdILg7tAh
FTuH7feVOpuWMCRgKWUQOMwGYxkn1LBQAAAMEAxbKtDCLBNxJhCBpB0h+KnX3F8GyOUj1z
9F6DGIQ+zuCrAhCHrliFtyT2XXZs9/EYXM92YDRDOHzN7iQ4cF1l9aA+AXIyvLXFbe2hUC
CMQ+3PDFipnRDJwRSNYop8UPjdbXtbgPASY1VVOUd0Kj3htw1gsKcMtvFDxcpeLGDPHR3T
EJ2LAxV3Ij8M0ScKOKKQ6DOUIlgcj8a218BzNbfh4B9o5MMstbESfNaiwytpdNImk/Nft8
BSEtS7V816N3MpAAAADnNjb3V0QFozNzBQLUQzAQIDBA==
-----END OPENSSH PRIVATE KEY-----
```

Публичный ключ `cat ~/.ssh/id_rsa.pub`:
```
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCfRIDBzqhKIki8fLAEhdCe3/m3eLTyuYou5wrHgholWZiYeg+V63qu2rLHlB6IQWEEQsX1aUUYI82g5DdDC4Pq2+ZZvEvDQ1XOToml96V2F47Ciz3UJfjPniS58M2evw0QUVBLeek/a1VQUt87cARa+/RvfTID9Zf7P45lX9QQ7glnwTUoeuSBtyYiWpwCGr/8bS+kYi2mi10Ou2T3OdLdXyXI6E1WPe5Y5u91Cf6r6Y0vw49hZwagLb/90hWs4EF4pYNyRTdlKXOXAsLctiNI+CVAWBe6b/6MWEXrdTKyUY9oqsF8k+2F60R3yHYSIfV2yWC5WROIDPETrVNRWRdyMi2PWPk6N2zAD+ouZDMoQnWcyr87c9q+D3un0xYklQnVFX81zmQyxKpPCOmWO7S00aGieLL1yDeHn1GUDMkteQWobKFKx9cHk3WLYq0jeZWq3dBN4Pf8R4q7CrtxfQ1UdaJDXpCecRtp9PuT+GYR9wLxT/wbN9JQYKmXaz7XQs0= scout@Z370P-D3
```
Либо возможен вариант, когда Вы заводите аккаунт на Gitlab и мы подключаем Ваш аккаунт к данному репозиторию.

### Техподдержка

По любым вопросам технического характера контакт Telegram: `@c2wqf13bd`.

Так как мы заинтересованы в том, чтобы работа была завершена в оговоренные сроки не меньше, чем Вы, то, пожалуйста, формулируйте
вопросы, соблюдая причинно-следственные связи, таким образом, чтобы в предложении содержались все
части речи русского языка и знаки препинания, и чтобы не возникало таких ситуаций, когда к каждому слову в
вопросе можно задать встречный вопрос.
